package edu.khtn.loto_offline;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Random;

import edu.khtn.object.PlayerList;

public class ReaderActivity extends Activity implements Serializable {
    final int numMax = 90;
    TextView[][] textList = new TextView[9][10];
    int[] numArr = new int[90];
    Intent intent;
    PlayerList playerList = new PlayerList();
    TextView[] textName = new TextView[6];
    TextView[] textCash = new TextView[6];
    TextView pricePer, readTime, textReader;
    Button btnRead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);
        linkView();
        intent = getIntent();
        pricePer.setText(intent.getStringExtra("pricePer"));
        readTime.setText(intent.getStringExtra("readTime"));
        playerList = (PlayerList) intent.getSerializableExtra("playerList");
        for (int i = 0; i < playerList.size(); i++) {
            String name = playerList.getPlayerList().get(i).getName();
            String cash = String.valueOf(playerList.getPlayerList().get(i).getCash());
            textName[i].setText(name + ": ");
            textCash[i].setText(cash);
        }
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rdm = random();
                textReader.setText(rdm + "");
                textList[(rdm - 1) / 10][(rdm - 1) % 10].setText(rdm + "");
            }
        });
    }

    public int random() {
        int num = 0;
        int sum = 0;
        Random rdm = new Random();
        for (int i = 0; i < 90; i++) {
            sum += numArr[i];
        }
        do {
            num = rdm.nextInt(numMax) + 1;
        } while (num == numArr[num - 1] && sum < 4095);
        numArr[num - 1] = num;
        return num;
    }

    public void thongBao(String noiDung, Boolean exitOrNot) {
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle("Thông Báo!");
        msg.setMessage(noiDung);
        if (exitOrNot.booleanValue()) {
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }

    public void linkView() {
        pricePer = (TextView) findViewById(R.id.text_pricePer);
        readTime = (TextView) findViewById(R.id.text_readTime);
        textReader = (TextView) findViewById(R.id.text_reader);
        textName[0] = (TextView) findViewById(R.id.text_name1);
        textName[1] = (TextView) findViewById(R.id.text_name2);
        textName[2] = (TextView) findViewById(R.id.text_name3);
        textName[3] = (TextView) findViewById(R.id.text_name4);
        textName[4] = (TextView) findViewById(R.id.text_name5);
        textName[5] = (TextView) findViewById(R.id.text_name6);
        textCash[0] = (TextView) findViewById(R.id.text_cash1);
        textCash[1] = (TextView) findViewById(R.id.text_cash2);
        textCash[2] = (TextView) findViewById(R.id.text_cash3);
        textCash[3] = (TextView) findViewById(R.id.text_cash4);
        textCash[4] = (TextView) findViewById(R.id.text_cash5);
        textCash[5] = (TextView) findViewById(R.id.text_cash6);
        btnRead = (Button) findViewById(R.id.btn_read);
        textList[0][0] = (TextView) findViewById(R.id.text_0_0);
        textList[0][1] = (TextView) findViewById(R.id.text_0_1);
        textList[0][2] = (TextView) findViewById(R.id.text_0_2);
        textList[0][3] = (TextView) findViewById(R.id.text_0_3);
        textList[0][4] = (TextView) findViewById(R.id.text_0_4);
        textList[0][5] = (TextView) findViewById(R.id.text_0_5);
        textList[0][6] = (TextView) findViewById(R.id.text_0_6);
        textList[0][7] = (TextView) findViewById(R.id.text_0_7);
        textList[0][8] = (TextView) findViewById(R.id.text_0_8);
        textList[0][9] = (TextView) findViewById(R.id.text_0_9);
        textList[1][0] = (TextView) findViewById(R.id.text_1_0);
        textList[1][1] = (TextView) findViewById(R.id.text_1_1);
        textList[1][2] = (TextView) findViewById(R.id.text_1_2);
        textList[1][3] = (TextView) findViewById(R.id.text_1_3);
        textList[1][4] = (TextView) findViewById(R.id.text_1_4);
        textList[1][5] = (TextView) findViewById(R.id.text_1_5);
        textList[1][6] = (TextView) findViewById(R.id.text_1_6);
        textList[1][7] = (TextView) findViewById(R.id.text_1_7);
        textList[1][8] = (TextView) findViewById(R.id.text_1_8);
        textList[1][9] = (TextView) findViewById(R.id.text_1_9);
        textList[2][0] = (TextView) findViewById(R.id.text_2_0);
        textList[2][1] = (TextView) findViewById(R.id.text_2_1);
        textList[2][2] = (TextView) findViewById(R.id.text_2_2);
        textList[2][3] = (TextView) findViewById(R.id.text_2_3);
        textList[2][4] = (TextView) findViewById(R.id.text_2_4);
        textList[2][5] = (TextView) findViewById(R.id.text_2_5);
        textList[2][6] = (TextView) findViewById(R.id.text_2_6);
        textList[2][7] = (TextView) findViewById(R.id.text_2_7);
        textList[2][8] = (TextView) findViewById(R.id.text_2_8);
        textList[2][9] = (TextView) findViewById(R.id.text_2_9);
        textList[3][0] = (TextView) findViewById(R.id.text_3_0);
        textList[3][1] = (TextView) findViewById(R.id.text_3_1);
        textList[3][2] = (TextView) findViewById(R.id.text_3_2);
        textList[3][3] = (TextView) findViewById(R.id.text_3_3);
        textList[3][4] = (TextView) findViewById(R.id.text_3_4);
        textList[3][5] = (TextView) findViewById(R.id.text_3_5);
        textList[3][6] = (TextView) findViewById(R.id.text_3_6);
        textList[3][7] = (TextView) findViewById(R.id.text_3_7);
        textList[3][8] = (TextView) findViewById(R.id.text_3_8);
        textList[3][9] = (TextView) findViewById(R.id.text_3_9);
        textList[4][0] = (TextView) findViewById(R.id.text_4_0);
        textList[4][1] = (TextView) findViewById(R.id.text_4_1);
        textList[4][2] = (TextView) findViewById(R.id.text_4_2);
        textList[4][3] = (TextView) findViewById(R.id.text_4_3);
        textList[4][4] = (TextView) findViewById(R.id.text_4_4);
        textList[4][5] = (TextView) findViewById(R.id.text_4_5);
        textList[4][6] = (TextView) findViewById(R.id.text_4_6);
        textList[4][7] = (TextView) findViewById(R.id.text_4_7);
        textList[4][8] = (TextView) findViewById(R.id.text_4_8);
        textList[4][9] = (TextView) findViewById(R.id.text_4_9);
        textList[5][0] = (TextView) findViewById(R.id.text_5_0);
        textList[5][1] = (TextView) findViewById(R.id.text_5_1);
        textList[5][2] = (TextView) findViewById(R.id.text_5_2);
        textList[5][3] = (TextView) findViewById(R.id.text_5_3);
        textList[5][4] = (TextView) findViewById(R.id.text_5_4);
        textList[5][5] = (TextView) findViewById(R.id.text_5_5);
        textList[5][6] = (TextView) findViewById(R.id.text_5_6);
        textList[5][7] = (TextView) findViewById(R.id.text_5_7);
        textList[5][8] = (TextView) findViewById(R.id.text_5_8);
        textList[5][9] = (TextView) findViewById(R.id.text_5_9);
        textList[6][0] = (TextView) findViewById(R.id.text_6_0);
        textList[6][1] = (TextView) findViewById(R.id.text_6_1);
        textList[6][2] = (TextView) findViewById(R.id.text_6_2);
        textList[6][3] = (TextView) findViewById(R.id.text_6_3);
        textList[6][4] = (TextView) findViewById(R.id.text_6_4);
        textList[6][5] = (TextView) findViewById(R.id.text_6_5);
        textList[6][6] = (TextView) findViewById(R.id.text_6_6);
        textList[6][7] = (TextView) findViewById(R.id.text_6_7);
        textList[6][8] = (TextView) findViewById(R.id.text_6_8);
        textList[6][9] = (TextView) findViewById(R.id.text_6_9);
        textList[7][0] = (TextView) findViewById(R.id.text_7_0);
        textList[7][1] = (TextView) findViewById(R.id.text_7_1);
        textList[7][2] = (TextView) findViewById(R.id.text_7_2);
        textList[7][3] = (TextView) findViewById(R.id.text_7_3);
        textList[7][4] = (TextView) findViewById(R.id.text_7_4);
        textList[7][5] = (TextView) findViewById(R.id.text_7_5);
        textList[7][6] = (TextView) findViewById(R.id.text_7_6);
        textList[7][7] = (TextView) findViewById(R.id.text_7_7);
        textList[7][8] = (TextView) findViewById(R.id.text_7_8);
        textList[7][9] = (TextView) findViewById(R.id.text_7_9);
        textList[8][0] = (TextView) findViewById(R.id.text_8_0);
        textList[8][1] = (TextView) findViewById(R.id.text_8_1);
        textList[8][2] = (TextView) findViewById(R.id.text_8_2);
        textList[8][3] = (TextView) findViewById(R.id.text_8_3);
        textList[8][4] = (TextView) findViewById(R.id.text_8_4);
        textList[8][5] = (TextView) findViewById(R.id.text_8_5);
        textList[8][6] = (TextView) findViewById(R.id.text_8_6);
        textList[8][7] = (TextView) findViewById(R.id.text_8_7);
        textList[8][8] = (TextView) findViewById(R.id.text_8_8);
        textList[8][9] = (TextView) findViewById(R.id.text_8_9);
    }
}
