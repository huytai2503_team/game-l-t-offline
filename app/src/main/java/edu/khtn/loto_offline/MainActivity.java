package edu.khtn.loto_offline;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.Serializable;

import edu.khtn.object.Player;
import edu.khtn.object.PlayerList;

public class MainActivity extends AppCompatActivity implements Serializable {
    PlayerList playerList = new PlayerList();
    EditText[] editArr = new EditText[6];
    Intent intentModels, intentReader;
    EditText firstCash, pricePer, readTime;
    Button btnOpenModels, btnOpenReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intentModels = new Intent(this, ModelsActivity.class);
        intentReader = new Intent(this, ReaderActivity.class);
        btnOpenModels = (Button) findViewById(R.id.btn_openModels);
        btnOpenReader = (Button) findViewById(R.id.btn_openReader);
        firstCash = (EditText) findViewById(R.id.edit_firstCash);
        pricePer = (EditText) findViewById(R.id.edit_pricePer);
        readTime = (EditText) findViewById(R.id.edit_readTime);
        editArr[0] = (EditText) findViewById(R.id.edit_name1);
        editArr[1] = (EditText) findViewById(R.id.edit_name2);
        editArr[2] = (EditText) findViewById(R.id.edit_name3);
        editArr[3] = (EditText) findViewById(R.id.edit_name4);
        editArr[4] = (EditText) findViewById(R.id.edit_name5);
        editArr[5] = (EditText) findViewById(R.id.edit_name6);
        btnOpenModels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentModels);
            }
        });
        btnOpenReader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = "";
                int cash = 0;
                for (int i = 0; i < 6; i++) {
                    if (editArr[i].getText().toString().isEmpty()==false) {
                        Player player = new Player();
                        name = editArr[i].getText().toString();
                        cash = Integer.parseInt(firstCash.getText().toString());
                        player.setName(name);
                        player.setCash(cash);
                        playerList.addPlayer(player);
                    }
                }
                intentReader.putExtra("pricePer", pricePer.getText().toString());
                intentReader.putExtra("readTime", readTime.getText().toString());
                intentReader.putExtra("playerList", playerList);
                startActivity(intentReader);
            }
        });
    }

    public void thongBao(String noiDung, Boolean exitOrNot) {
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle("Thông Báo!");
        msg.setMessage(noiDung);
        if (exitOrNot.booleanValue()) {
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }
}
