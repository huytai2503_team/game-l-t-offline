package edu.khtn.object;

import java.io.Serializable;

public class Player implements Serializable{
    public Player() {
        super();
    }

    public Player(String name) {
        this.name = name;
        this.cash = 0;
    }

    public Player(String name, int cash) {
        this.name = name;
        this.cash = cash;
    }

    String name;
    int cash;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public String toString() {
        return "(" + this.name + "," + this.cash + ")";
    }
}
